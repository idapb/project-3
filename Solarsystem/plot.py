from numpy import *
import matplotlib.pyplot as plt

distance = []
for filename in ['Verlet.txt','RK4.txt']:

   A = loadtxt(filename,skiprows=1)
   n = len(A[0,:])
   m = len(A[:,0])
   years, dt = fromfile(filename,count=2,sep=' ')

   plt.figure()
   for i in range(0,n,2):
       plt.plot(A[:,i],A[:,i+1])
       plt.hold('on')
   plt.title('%s, %g years, timestep: %g' % (filename.split('.')[0],years,dt))
   plt.xlabel('Position [AU]')
   plt.ylabel('Position [AU]')
   #plt.axis([-2,2,-2,2])
   plt.savefig('%s.pdf' % (filename.split('.')[0]), bbox_inches='tight')

#   dist = sqrt(A[:,0]**2 + A[:,1]**2)
#   distance.append(dist)
#
#x = linspace(0,years,len(distance[0]))
#plt.figure()
#plt.plot(x,distance[0],label='verlet')
#plt.hold('on')
#plt.plot(x,distance[1],label='rk4')
#plt.legend()
#plt.xlabel('time [years]')
#plt.ylabel('distance [Au]')
#plt.title('Distance of earth from center of mass, %g years, timestep %g' % (years,dt))
#
plt.show()
