#include "system.h"
#include <iomanip>
#include <fstream>

using namespace std;
using namespace arma;

System::System(double timestep)
{
  dt = timestep;
  nb_objects = 0;
}

void System::addobject(Object body)
{
  objectlist[nb_objects] = body;
  nb_objects += 1;
}

vec System::rk4()
{
  vec k1(4*n), k2(4*n), k3(4*n), k4(4*n);
  k1 = dt*forces_rk4(A);
  k2 = dt*forces_rk4(A + k1*0.5);
  k3 = dt*forces_rk4(A + k2*0.5);
  k4 = dt*forces_rk4(A + k3);
  A += (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
  return A;
}

vec System::forces_rk4(vec B)
{
  vec forces = zeros(2*n);
  vec rhs = zeros(4*n);
  for(i=0;i<n;i++)
    {
    for(j=i+1;j<n;j++)
      {
	double x = B(4*i) - B(4*j);
	double y = B(4*i+1) - B(4*j+1);
	double r = sqrt(x*x + y*y);
	double f = -G*objectlist[i].mass*objectlist[j].mass/(r*r*r);
	forces(2*i) += f*x;
	forces(2*i+1) += f*y;
	forces(2*j) += -f*x;
	forces(2*j+1) += -f*y;
      }
    }
    for(i=0;i<n;i++)
      {
	rhs(4*i) = B(4*i+2);
	rhs(4*i+1) = B(4*i+3);
	rhs(4*i+2) = forces[2*i]/objectlist[i].mass;
	rhs(4*i+3) = forces[2*i+1]/objectlist[i].mass;
      }
return rhs;
}

void System::initialize_rk4()
{
  n = nb_objects;
  A = zeros(4*n);
  for(i=0;i<n;i++)
    {
      A(4*i) = objectlist[i].pos(0);
      A(4*i+1) = objectlist[i].pos(1);
      A(4*i+2) = objectlist[i].vel(0);
      A(4*i+3) = objectlist[i].vel(1);
    }
}

void System::energies(vec &kinetic_energy, vec &potential_energy, vec &angular_momentum)
{
  vec U = zeros(n);
  for(i=0;i<n;i++)
    {
      for(j=i+1;j<n;j++)
	{
	  double x = A(4*i) - A(4*j);
	  double y = A(4*i+1) - A(4*j+1);
	  double r = sqrt(x*x + y*y);
	  double U_temp = G*objectlist[i].mass*objectlist[j].mass/r*0.5;
	  U(i) -= U_temp;
	  U(j) -= U_temp;
	}
    }
  for(i=0;i<n;i++)
    {
      potential_energy(i) = U(i);

      double v = sqrt(A(4*i+2)*A(4*i+2)+A(4*i+3)*A(4*i+3));
      kinetic_energy(i) = 0.5*objectlist[i].mass*v*v;

      angular_momentum(i) = (A(4*i)*A(4*i+3)-A(4*i+1)*A(4*i+2))*objectlist[i].mass;
    }
}

vec System::verlet()
{
  vec pos_new = pos;
  pos = 2*pos - pos_prev + dt*dt*forces_verlet(pos);
  pos_prev = pos_new;
  return pos;
}

vec System::forces_verlet(vec B)
{
  vec forces = zeros(2*n);
  vec rhs = zeros(2*n);
  for(i=0;i<n;i++){
    for(j=i+1;j<n;j++)
      {
	double x = B(2*i) - B(2*j);
	double y = B(2*i+1) - B(2*j+1);
        double r = sqrt(x*x + y*y);
	double f = -G*objectlist[i].mass*objectlist[j].mass/(r*r*r);
	forces(2*i) += f*x;
	forces(2*i+1) += f*y;
	forces(2*j) += -f*x;
	forces(2*j+1) += -f*y;
}}
    for(i=0;i<n;i++)
      {
	rhs(2*i) = forces[2*i]/objectlist[i].mass;
	rhs(2*i+1) = forces[2*i+1]/objectlist[i].mass;
      }
return rhs;
}

void System::initialize_verlet()
{
  n = nb_objects;
  pos = zeros(2*n);
  for(i=0;i<n;i++)
    {
      pos(2*i) = objectlist[i].pos(0);
      pos(2*i+1) = objectlist[i].pos(1);
    }
  vec vel = zeros(2*n);
  for(i=0;i<n;i++)
    {
      vel(2*i) = objectlist[i].vel(0);
      vel(2*i+1) = objectlist[i].vel(1);
    }
  pos_prev = pos - dt*vel;
}
