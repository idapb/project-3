#include "object.h"

using namespace arma;
using namespace std;

Object::Object() {}

Object::Object(double m, vec x, vec v)
{
  mass = m;
  pos = x;
  vel = v;
}
