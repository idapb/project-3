#ifndef SYSTEM_H
#define SYSTEM_H
#include "object.h"
#include <armadillo>
#include <cmath>

const double pi = 4*atan(1.0);
const double G = 4*pi*pi;

class System
{
  double dt;
  vec A, pos, pos_prev, kinetic_energy, potential_energy, angular_momentum;
  int i, j, n, nb_objects;
  public:
  Object objectlist[100];
  System (double);
  void addobject(Object);
  vec rk4();
  vec verlet();
  void initialize_verlet();
  void initialize_rk4();
  vec forces_rk4(vec);
  vec forces_verlet(vec);
  void energies(vec&,vec&,vec&);
};

#endif
