#ifndef SYSTEM_H
#define SYSTEM_H
#include "object.h"
#include <armadillo>
#include <cmath>
#include <vector>
#include <omp.h>

using std::vector;

const double pi = 4*atan(1.0);
const double G = pi*pi/(8000);

class System
{
  double dt, tau, eps_min, eps_max, t, tol;
  vec A, pos, vel, kinetic_energy, potential_energy;
  int i, j, n, nb_objects, count;
  bool no_value;
  double c1 = 1.0/4.0;
  double c2 = 3.0/32.0;
  double c3 = 9.0/32.0;
  double c4 = 1932.0/2197.0;
  double c5 = 7200.0/2197.0;
  double c6 = 7296.0/2197.0;
  double c7 = 439.0/216.0;
  double c8 = 8.0;
  double c9 = 3680.0/513.0;
  double c10 = 845.0/4104.0;
  double c11 = 8.0/27.0;
  double c12 = 2.0;
  double c13 = 3544.0/2565.0;
  double c14 = 1859.0/4104.0;
  double c15 = 11.0/40.0;
  double c16 = 25.0/216.0;
  double c17 = 1408.0/2565.0;
  double c18 = 2197.0/4104.0;
  double c19 = 1.0/5.0;
  double c20 = 16.0/135.0;
  double c21 = 6656.0/12825.0;
  double c22 = 28561.0/56430.0;
  double c23 = 9.0/50.0;
  double c24 = 2.0/55.0;
public:
  vector<Object> objectlist;
  System (double,double);
  void addobject(double, vec, vec);
  void initialize();
  vec forces(vec);
  void rk4();
  void solve();
  void rk45();
  void energies();
  void verlet();
  vec forces_verlet(vec);
  void initialize_verlet();
  void verlet_solve();
  void removing_particles();
};

#endif
