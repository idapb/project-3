#include "system.h"
#include <iomanip>
#include <fstream>

using namespace std;
using namespace arma;

System::System(double timestep, double time)
{
  nb_objects = 0;
  dt = timestep;
  tau = time;
}

void System::addobject(double mass, vec pos, vec vel)
{
  Object x = Object(mass, pos, vel);
  objectlist.push_back(x);
  nb_objects += 1;
}

void System::energies()
{
  for(int i=0;i<n;i++)
    {
      for(int j=i+1;j<n;j++)
	{
          double x = A(6*i) - A(6*j);
	  double y = A(6*i+1) - A(6*j+1);
	  double z = A(6*i+2) - A(6*j+2);
	  double r = sqrt(x*x + y*y + z*z);
	  double pot_energy = G*objectlist[i].mass*objectlist[j].mass/r*0.5;
	  potential_energy(i) -= pot_energy;
	  potential_energy(j) -= pot_energy;
	}
    }
  double energy_tot = 0;
  for(int i=0;i<n;i++)
    {
      double v_squared = A(6*i+3)*A(6*i+3) + A(6*i+4)*A(6*i+4) + A(6*i+5)*A(6*i+5);
      kinetic_energy(i) = 0.5*objectlist[i].mass*v_squared;
      double energy = potential_energy(i) + kinetic_energy(i);
      energy_tot += energy;
    }
  cout << "t: " << t << "   " << "dt: " << dt << "   " << "energy tot: " << energy_tot <<  endl;
}

void System::removing_particles()
{
  for(i=0;i<n;i++)
    {
      double energy = potential_energy(i) + kinetic_energy(i);
      double r = sqrt(A(6*i)*A(6*i) + A(6*i+1)*A(6*i+1) + A(6*i+2)*A(6*i+2));
      if(energy > 0 and r > 5)
	{
	  objectlist.erase(objectlist.begin() + i);
	  n -= 1;
	  vec temp = zeros(6*n);
	  int k = 0;
	  for(int j=0;j<n;j++)
	    {
	      temp(6*j) = A(6*(j+k));
	      temp(6*j+1) = A(6*(j+k)+1);
	      temp(6*j+2) = A(6*(j+k)+2);
	      temp(6*j+3) = A(6*(j+k)+3);
	      temp(6*j+4) = A(6*(j+k)+4);
	      temp(6*j+5) = A(6*(j+k)+5);
	      if(j != i)
		  {
		    k = 1;
		  }
	    }
	  A.set_size(n);
	  A = temp;
	}
    }
}


void System::rk4()
{
  vec k1(6*n), k2(6*n), k3(6*n), k4(6*n);
  k1 = dt*forces(A);
  k2 = dt*forces(A + k1*0.5);
  k3 = dt*forces(A + k2*0.5);
  k4 = dt*forces(A + k3);
  A += (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
  t += dt;
  cout << t << endl;
}

void System::rk45()
{
  vec k1(6*n), k2(6*n), k3(6*n), k4(6*n), k5(6*n), k6(6*n);
  k1 = forces(A)*dt; 
  k2 = forces(A + c1*k1)*dt; 
  k3 = forces(A + c2*k1 + c3*k2)*dt; 
  k4 = forces(A + c4*k1 - c5*k2 + c6 * k3)*dt;
  k5 = forces(A + c7*k1 - c8*k2 + c9*k3 - c10*k4)*dt;  
  k6 = forces(A - c11*k1 + c12*k2 - c13*k3 + c14*k4 - c15*k5)*dt; 
  vec a = A + c16*k1 + c17*k3 + c18*k4 - c19*k5; 
  vec b = A + c20*k1 + c21*k3 + c22*k4 - c23*k5 + c24*k6;
  vec diff = abs(a-b);
//  double error = 0;
//  vec diff_pos = zeros(3*n);
//  for(i=0;i<n;i++)
//    {
//      diff_pos(i) = diff(i);
//      diff_pos(i+1) = diff(i+1);
//      diff_pos(i+2) = diff(i+2);
//    }
//  error = max(diff_pos);
  //cout << endl;
  //cout << "error: " << error << endl;
  //cout << "dt: " << dt << endl;
  //cout << "t: " << t << endl;
  double s = pow(tol*0.5*dt/max(diff),0.25);
  //cout << "dt*s: " << dt*s << endl;
  if(dt < 1e-7)
    {
      t += dt;
      dt = 1e-7;
      A = a;
      //cout << dt << endl;
    }
  else
    {
      A = a;
      t += dt;
      dt *= s;
      //cout << dt << endl;
    }
//  cout << max(diff) << endl;
//  if(max(diff) > 1e-15)
//    {
//    dt *= s;
//    }
//  else if(max(diff) < 1e-20)
//    {
//      dt *= s;
//    }
//  else
//    {
//      t += dt;
//      dt *= s;
//      A = a;
//      no_value = false;
//    }
  //  if(
//  else
//    {
//      A = a;
//      t += dt;
//      //dt *= s;
//      no_value = false;
//    }
//  if(dt < dt*s - tol and count < 30)
//    {
//      //dt *= s;
//      dt *= 2;
//      cout << "up" << endl;
//      count += 1;
//      //A = a;
//      //no_value = false;
//    }
//  else if(dt > dt*s + tol and count < 30)
//    {
//      //dt *= s;
//      dt *= 0.5;
//      cout << "down" << endl;
//      count += 1;
//      //A = a;
//      //no_value = false;
//    }
//  else
//    {
//      A = a;
//      t += dt;
//      //dt *= s;
//      no_value = false;
//    }
}

vec System::forces(vec B)
{
  vec forces = zeros(3*n);
  double x,y,z,r,f;

  for(int i=0;i<n;i++)
    {
      double fx = 0;
      double fy = 0;
      double fz = 0;
#pragma omp parallel for reduction(+:fx, fy,fz) shared(forces) private(x,y,z,r,f) num_threads(2)
      for(int j=i+1;j<n;j++)
	{
	  x = B(6*i) - B(6*j);
	  y = B(6*i+1) - B(6*j+1);
	  z = B(6*i+2) - B(6*j+2);
	  r = sqrt(x*x + y*y + z*z);
	  f = -G*objectlist[i].mass*objectlist[j].mass/(r*(r*r + 1e-5));
	  fx += f*x;
	  fy += f*y;
	  fz += f*z;
	  forces(3*j) += -f*x;
	  forces(3*j+1) += -f*y;
	  forces(3*j+2) += -f*z;
	}
    forces(3*i) += fx;
    forces(3*i+1) += fy;
    forces(3*i+2) += fz;
    }
    for(i=0;i<n;i++)
      {
	B(6*i) = B(6*i+3);
	B(6*i+1) = B(6*i+4);
	B(6*i+2) = B(6*i+5);
	B(6*i+3) = forces[3*i]/objectlist[i].mass;
	B(6*i+4) = forces[3*i+1]/objectlist[i].mass;
	B(6*i+5) = forces[3*i+2]/objectlist[i].mass;
      }
    return B;
}

void System::initialize()
{
  n = nb_objects;
  A = zeros(6*n);
  for(i=0;i<n;i++)
{
  A(6*i) = objectlist[i].pos(0);
  A(6*i+1) = objectlist[i].pos(1);
  A(6*i+2) = objectlist[i].pos(2);
  A(6*i+3) = objectlist[i].vel(0);
  A(6*i+4) = objectlist[i].vel(1);
  A(6*i+5) = objectlist[i].vel(2);
 }
}

void System::solve()
{
  ofstream myfile;
  myfile.open("rk4.txt");
  myfile << tau << " " << endl;
  ofstream myfile2;
  myfile2.open("energies.txt");
  initialize();
  tol = 1e-7;
  t = 0;
  int t_temp = 0;
  int k = 0;
  kinetic_energy = zeros(n);
  potential_energy = zeros(n);
  //energies();
  while(t <= tau)
    {
      //      rk4();
//      count = 0;
//      no_value = true;
//      while(no_value)
//	{
//	  rk45();
//	}
      rk45();
      if(t_temp == 100*k)
	{
	  kinetic_energy.set_size(n);
	  potential_energy.set_size(n);
	  kinetic_energy.zeros();
	  potential_energy.zeros();
	  energies();
	  for(i=0;i<n;i++)
	    {
	      myfile << setprecision(4) << A(6*i) << " " << A(6*i+1) << " " << A(6*i+2) << " ";
	      myfile2 << setprecision(4) << kinetic_energy(i) << " " << potential_energy(i) << " ";
	    }
	  removing_particles();
	  k += 1;
	  myfile << endl;
	  myfile2 << endl;
	  cout << "number of particles remaining: " << n << endl;
	}
      t_temp += 1;
    }
  removing_particles();
  cout << "number of particles remaining: " << n << endl;
  myfile.close();
  myfile2.close();
}

void System::verlet()
{
  vec pos_new = pos + vel*dt + 0.5*forces_verlet(pos)*dt*dt;
  vel += 0.5*(forces_verlet(pos) + forces_verlet(pos_new))*dt;
  pos = pos_new;
  for(i=0;i<n;i++)
    {
      A(6*i) = pos(i);
      A(6*i+1) = pos(i+1);
      A(6*i+2) = pos(i+2);
      A(6*i+3) = vel(i);
      A(6*i+4) = vel(i+1);
      A(6*i+5) = vel(i+2);
    }
}
  
vec System::forces_verlet(vec B)
{
  vec forces = zeros(3*n);
  for(i=0;i<n;i++){
    for(j=i+1;j<n;j++)
      {
	double x = B(3*i) - B(3*j);
	double y = B(3*i+1) - B(3*j+1);
	double z = B(3*i+2) - B(3*j+2);
        double r = sqrt(x*x + y*y + z*z);
	double f = -G*objectlist[i].mass*objectlist[j].mass/(r*(r*r+ 1e-9*1e-9));
	forces(3*i) += f*x;
	forces(3*i+1) += f*y;
	forces(3*i+2) += f*z;
	forces(3*j) += -f*x;
	forces(3*j+1) += -f*y;
	forces(3*j+2) += -f*z;
      }
  }
    for(i=0;i<n;i++)
      {
	B(3*i) = forces[3*i]/objectlist[i].mass;
	B(3*i+1) = forces[3*i+1]/objectlist[i].mass;
	B(3*i+2) = forces[3*i+2]/objectlist[i].mass;
      }
    return B;
}

void System::initialize_verlet()
{
  pos = zeros(3*n);
  for(i=0;i<n;i++)
    {
      pos(3*i) = objectlist[i].pos(0);
      pos(3*i+1) = objectlist[i].pos(1);
      pos(3*i+2) = objectlist[i].pos(2);
    }
  vel = zeros(3*n);
  for(i=0;i<n;i++)
    {
      vel(3*i) = objectlist[i].vel(0);
      vel(3*i+1) = objectlist[i].vel(1);
      vel(3*i+2) = objectlist[i].vel(2);
    }
}

void System::verlet_solve()
{
  ofstream myfile;
  myfile.open("verlet.txt");
  myfile << tau << " " << endl;
  ofstream myfile2;
  myfile2.open("energies.txt");
  initialize();
  initialize_verlet();
  t = 0;
  tol = 1e-5;
  int k = 0;
  int t_temp = 0;
  while(t <= tau)
    {
      //rk45();
      verlet();
      t += dt;
      cout << t << endl;
      if(t_temp == 100*k)
	{
	  kinetic_energy.set_size(n);
	  potential_energy.set_size(n);
	  kinetic_energy.zeros();
	  potential_energy.zeros();
	  energies();
	  k += 1;
      for(i=0;i<n;i++)
	{
	  myfile << setprecision(4) << pos(3*i) << " " << pos(3*i+1) << " " << pos(3*i+2) << " ";
	  myfile2 << setprecision(4) << kinetic_energy(i) << " " << potential_energy(i) << " ";
	}    
      myfile << endl;
      myfile2 << endl;
	}
      cout << "particles remaining: " << n << endl;
      t_temp += 1;
    }
  energies();
  for(i=0;i<n;i++)
    {
      myfile << setprecision(4) << pos(3*i) << " " << pos(3*i+1) << " " << pos(3*i+2) << " ";
      myfile2 << setprecision(4) << kinetic_energy(i) << " " << potential_energy(i) << " ";
    }    
  myfile.close();
  myfile2.close();
}

