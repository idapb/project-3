from numpy import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#for filename in ['rk4.txt','verlet.txt']:
'''
filename = 'rk4.txt'
A = loadtxt(filename,skiprows=1)
n = len(A[0,:])
N = len(A[:,0])
time = fromfile(filename,count=1,sep=' ')


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
fig3 = plt.figure()
ax3 = fig3.add_subplot(111, projection='3d')

for i in range(0,n,3):
      ax.plot(A[N/2::5,i], A[N/2::5,i+1], A[N/2::5,i+2])
      #ax.plot(A[:,i], A[:,i+1], A[:,i+2])
      ax.hold('on')
      l = 1
      ax.auto_scale_xyz([-l,l], [-l, l], [-l, l])
      plt.title('%s, %g tau_crunch' % (filename.split('.')[0],time))
      plt.xlabel('r0')
      plt.ylabel('r0')
      ax.hold('off')
      ax2.plot(A[0:2,i], A[0:2,i+1], A[0:2,i+2], 'o')
      ax3.plot(A[165:167,i], A[165:167,i+1], A[165:167,i+2], 'o')
plt.show()
'''
A = []
filename = open('energies.txt','r')
for line in filename:
      A.append(asarray(map(float,line.strip().split())))
A = asarray(A)
n = len(A[:][0])

print sum(A[-1][::2])*2
print -sum(A[-1][1::2])

virial = zeros(n)
line = zeros(n)
for i in range(n):
      virial1 = sum(A[i][::2])*2
      virial2 = -sum(A[i][1::2])
      virial[i] = virial1/virial2
      line[i] = 1
x = linspace(0,7.5,n)

plt.plot(x,virial)
plt.hold('on')
plt.plot(x,line)
plt.show()

A = []
filename = open('rk4.txt')
for line in filename:
      A.append(asarray(map(float,line.strip().split())))
A = asarray(A)
B = A[-1]
rad = []
for i in range(0,len(B),3):
      r = sqrt(B[i]**2 + B[i+1]**2 + B[i+2]**2)
      rad.append(r)
rad = asarray(rad)
#print rad
plt.hist(rad,20)
plt.show()

