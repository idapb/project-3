\documentclass[11pt,a4wide]{report}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage[dvips]{epsfig}
%\usepackage[T1]{fontenc}
%\usepackage{cite} % [2,3,4] --> [2--4]
%\usepackage{shadow}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}

%\setcounter{tocdepth}{2}

\lstset{language=c++}
%\lstset{alsolanguage=[90]Fortran}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\begin{document}

\title{FYS - 4150}
\author{Candidate number 53}
\maketitle

\section*{Introduction}
The aim of this project is to first simulate the solar system using Newtonian gravity. This will be solved with the Runge-Kutta 4 method and the Verlet method, in order to compare the stability and precision of these two methods.

We will then modify our program to simulate a galactic cluster with a large number of particles. The aim is to look at a simple model for an open cluster made from the gravitational collapse and interaction among a large number of stars. We will study the statistical properties of the collapsed system.\\\\
The source codes for all the programs can be found at \\\\
https://bitbucket.org/CandidateNumber53/astronomy-project.

\section*{Theory}
The gravitational force between two objects is given by:
%\begin{equation}
\begin{equation}
F_G=\frac{GMm}{r^2},
\label{eq:force}
\end{equation}
%\end{equation}
where $M$ is the mass of object 1, and $m$ is the mass of object 2. The gravitational constant is $G$ and $r$ is the distance between the objects.\\\\
From Newton's second law we get the motion of an object with mass $m$ as a second-order ordinary differential equation:
\[
\frac{d^2x}{dt^2}=\frac{F_G}{m} = a,
\]
where $x$ is the position of the object and $a$ is the acceleration. \\\\
We can rewrite this as a set of coupled first order differential eguations:
\begin{equation}
\frac{dv}{dt}=a
\:\:\:\:\text{and}\:\:\:\:
\frac{dx}{dt}=v
\label{eq:set}
\end{equation}
where $v$ is the velocity of the object.\\\\
The potential energy for gravitational forces between to objects is given by:
\begin{equation}
E_p = -\frac{GMm}{r},
\label{eq:pot}
\end{equation}
the kinetic energy of a point object, or non-rotating object, is given by:
\begin{equation}
E_k = \frac{1}{2}mv²
\label{eq:kin}
\end{equation}
and the angular momentum is:
\begin{equation}
\vec{L} = \vec{r} \times m\vec{v},
\label{eq:ang}
\end{equation}
where $r$ is the distance to the point the object is rotating around.\\
In two dimensions this can be written:
\[
L = m(xv_y - yv_x)
\]
The virial theorem says that for a bound gravitational system in equilibrium we have
\[
2<K> = -<V>,
\]
where $<K>$ is the average kinetic energy over time and $<V>$ is the average potential energy over time. By the ergodic hypothesis we can take the average over a large system instead of the average over time.\\\\
The radial distribution of particles in an equilibrium state can be fit with the expression
\begin{equation}
n(r) = \frac{n_0}{\left(1+\left(\frac{r}{r_0}\right)^{4}\right)},
\label{eq:rad}
\end{equation}
where $r$ is radial distance.

\subsection*{Runge Kutta 4}
The Runge Kutta 4 method (RK4) is an iterative method for finding approximate solutions to ordinary differential equations using a weighted average of four increments when calculating the value at the next timestep. \\
We start with an equation
\[
\frac{dy}{dt} = f(t,y),
\]
where the initial value $y(t_0)$ is given. We then pick a step-size $h$ and define 
\[
y_{i+1} = y_i + \frac{h}{6}(k_1 + 2k_2 + 2k_3 + k_4)
\]
\[
t_{i+1} = t_i + h,
\]
where
\begin{align*}
&k_1 = f(t_i, y_i)
\\
&k_2 = f(t_i + h/2, y_i + k_1/2)
\\
&k_3 = f(t_i + h/2, y_i + k_2/2)
\\
&k_4 = f(t_i + h, y_i + k_3)
\end{align*}
Here $k_1$ is based on the slope at $t_i$, $k_2$ is based on the slope at the midpoint, $k_3$ is based on the improved slope at the midpoint, and $k_4$ is based on the slope at the end of the interval.\\
This method has a global truncation error of the order $O(h^4)$.

\subsection*{Runge Kutta 45}
Runge Kutta 45 is an adaptive timestep method. It uses Runge Kutta of order four and of order five to calculate approximations to the value at a given timestep. The optimal timestep is then computed based on the difference between the approximations, and this timestep is used in the next iteration.\\\\
For each time step we calculate
\begin{align*}
&k_1 = hf(t_i,y_i)
\\
&k_2 = hf(t_i+\frac{1}{4}h,y_i+\frac{1}{4}k_1)
\\
&k_3 = hf(t_i+\frac{3}{8}h,y_i+\frac{3}{32}k_1+\frac{9}{32}k_2)
\\
&k_4 = hf(t_i+\frac{12}{13}h,y_i+\frac{1932}{2197}k_1-\frac{7200}{2197}k_2+\frac{7296}{2197}k_3)
\\
&k_5 = hf(t_i+h,y_i+\frac{439}{216}k_1-8k_2+\frac{3680}{513}k_3-\frac{845}{4104}k_4)
\\
&k_6 = hf(t_i+\frac{1}{2}h,y_i-\frac{8}{27}k_1+2k_2+\frac{3544}{2565}k_3+\frac{1859}{4104}k_4-\frac{11}{40}k_5)
\end{align*}
The approximation given by Runge Kutta 4 is
\[
a = y_i+\frac{25}{216}k_1+\frac{1408}{2565}k_3+\frac{2197}{4104}k_4-\frac{1}{5}k_5
\]
and the approximation given by Runge Kutta 5 is
\[
b = y_i+\frac{16}{135}k_1+\frac{6656}{12825}k_3+\frac{28561}{56430}k_4-\frac{9}{50}k_5+\frac{2}{55}k_6
\]
The optimal timestep is then given by $hs$, where $h$ is the previous timestep and
\[
s = \left(\frac{\epsilon h}{2|a-b|}\right)^{\frac{1}{4}}
\]
$\epsilon$ is a chosen tolerance.


\subsection*{Verlet}
The Verlet method is used to calculate second order differential equations like Newton's second law. Consider equation \ref{eq:set}, with given initial values and timestep $h$. The discretized positions $x_i$ can be calculated from
\[
x_{i+1} = 2x_i - x_{i-1} + h^2a_i
\]
We do not need to calculate the velocities in this algorithm in order to find the positions.\\
The truncation error is of the order $O(h^4)$.\\\\
A related algorithm is the Velocity Verlet method which calculates both velocity and positions.
\[
x_{i+1} = x_i + v_ih + \frac{1}{2}a_ih^2
\]
\[
v_{i+1} = v_i + \frac{a_i + a_{i+1}}{2}h
\]
The error is of the same order as the basic Verlet method.



\section*{Method}
\subsection*{Solar System}
We started writing our code only for the earth orbiting the sun. This was done by solving the set of coupled first order differential equations given by equation \ref{eq:set}, using the RK4 method and the Verlet method. We used astronomical units, solar masses and years as units of length, mass, and time respectively. 

We put the sun at rest in the origin and made plots of the motion of the Earth orbiting the Sun. The earth was given a velocity that led to a circular orbit. We also made a function to calculate the energies and angular momentum to see if these were constant. These values were printed to screen. 

In order to test the stability of the two methods for different time steps, we plotted the distance between the Sun and the Earth as a function of time.\\\\
Jupiter was then added to the simulation. In order to further test the stability of Verlet and RK4, Jupiter's mass was increased by a factor of 10 and 1000, and the results were plotted.

We changed the origin from the position of the sun to the center-of-mass, and gave the Sun an initial velocity making the total momentum of the system zero, so that the center of mass remained fixed.\\\\
Finally we added all the other planets, including Pluto, to the simulation.

\subsection*{Galactic Cluster}
We changed the program to do simulations in three dimensions instead of two, and changed the Verlet method to the Velocity-Verlet. We then implemented the two-body problem and tested the two methods for different timesteps and for different lengths of time. The time usage for each method was printed to screen.\\\\
We implemented Runge Kutta 45 as an adaptive timestep method in order to simulate a galaxy cluster. The code was extended to an arbitrary number of particles starting at rest with a uniform random distribution within a sphere. The units were changed to $20ly$ for length and $\tau_{crunch} = \sqrt{\frac{3\pi}{32G\rho_0}}$ for time, where $\rho_0$ is the initial particle density. $\tau_{crunch}$ is the time in which a system collapses into a singularity.\\\\
We then did a number of simulations to study the system. Potential and kinetic energy was calculated to see if we had energy conservation. This was used as a measurement for finding the best $\epsilon$ for the adaptive timestep method. The energy was also used to see if particles were ejected from the system, since bound particles will have negative energy and particles that are being ejected will have positive energy. Particles that were ejected were removed from the simulation. 

The virial theorem for the system was plotted as a function of time, and this was used to see if the system reached an equilibrium.\\\\
We introduced a smoothing function when calculating the force to get better energy conservation. The force was then given by
\[
F_{mod} = -\frac{GMm}{r^2 + \epsilon^2},
\]
where the value for $\epsilon$ that gave the best energy conservation was found by trial and error.\\\\
The differential solver was parallelized using OpenMP to get the program running faster.

\section*{Results}
\subsection*{Solarsystem}
The motion of the Earth orbiting the Sun with the sun kept fixed at the origin is shown in figure \ref{fig:circ} for Verlet and RK4. They both show a circular orbit.

Table \ref{tab:1} shows the kinetic energy, potential energy and angular momentum of the Earth in circular orbit at three different moments in time. We see that they are constant. 

In figure \ref{fig:dist10} we see the distance between the Sun and the Earth as a function of time for two different timesteps over a period of ten years. For RK4 the position seems quite stable, whereas for Verlet the position oscillates. The oscillation increases by the same factor as the timestep.

Figure \ref{fig:dist5000} shows the same distance over a period of 5000 years for one timestep. We see that RK4 starts out very accurate but then starts to oscillate leading to an error which increases gradually with time. Verlet starts out less precise but then the error begins to oscillate and increases on the same scale as RK4.\\\\

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rk4earth.pdf}
  %\caption{rk4}
  \label{fig:circsub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{verletearth.pdf}
  %\caption{verlet}
  \label{fig:circsub2}
\end{subfigure}
\caption{The position of Earth orbiting the Sun with velocity $2\pi$ which gives circular motion.}
\label{fig:circ}
\end{figure}

\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|c|}
    \hline
    Time [years] & 0 & 10 & 100 \\ \hline
    Kinetic energy $[m_{\odot}AU^2/Years^2]$ & $5.92\times10^{-5}$ & $5.92\times10^{-5}$ & $5.92\times10^{-5}$ \\ \hline
    Potential energy $[m_{\odot}AU^2/Years^2]$ & -$1.18\times10^{-5}$ & -$1.18\times10^{-5}$ & -$1.18\times10^{-4}$ \\ \hline
    Angular momentum $[m_{\odot}AU^2/Years]$ & $1.89\times10^{-5}$ & $1.89\times10^{-5}$ & $1.89\times10^{-5}$ \\ \hline
  \end{tabular}
  \caption{Energies and angular momentum for Earth in circular orbit around the Sun. \label{tab:1}}
\end{table}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{dist0,01-10.pdf}
  %\caption{rk4}
  \label{fig:01-10}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{dist0,001-10.pdf}
  %\caption{verlet}
  \label{fig:001-10}
\end{subfigure}
\caption{Comparison of the Verlet and Runge Kutta 4 methods for two different time-steps and for a short period of time.}
\label{fig:dist10}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.9\textwidth]{dist0,01-5000.pdf}
  \caption{Comparison of the Verlet and Runge Kutta 4 methods over a period of 5000 years. \label{fig:dist5000}}
\end{figure}

Figure \ref{fig:sunorigin} shows Jupiter and Earth in orbit around the sun. We see that adding Jupiter to the simulation gives the whole system a velocity upwards. For RK4 the system moves straight upwards, but for Verlet Earth's orbit also has a sideways motion.

In figure \ref{fig:incr10} the mass of Jupiter has been increased by a factor 10. This gives the system a greater velocity upwards. Again for RK4 the system moves in a straight line whereas for Verlet the motion of Earth moves slightly sideways.

In figure \ref{fig:jupiterx1000} Jupiter's mass has been increased by a factor 1000. The figure shows six different plots for Verlet and RK4 with three different timesteps. In all cases the Earth escapes the system after only a few orbits.

Figure \ref{fig:massorigin} shows the three-body system with the center-of-mass as origin and where the Sun is given an initial velocity. Only the Verlet method is shown, as RK4 gives identical plot. We see that now the origin is remained fixed and the Sun also moves in a circular orbit around the center-of-mass.

The positions of the tree-body system over a period of one thousand years is shown in figure \ref{fig:massorigin1000} for both Verlet and RK4. We see that for the Verlet method the orbit of the Earth gets less accurate with time. For RK4 the orbit remains stable.\\\\
 The whole solarsystem is plotted in figure \ref{fig:solarsystem} for Verlet and RK4. The plots for the two methods are identical but the plot for RK4 is zoomed in to show the motion of the four innermost planets.

In figure \ref{fig:solarsystemlargedt} we see the four innermost planets for a larger timestep. For RK4 we see that Mercury escapes the solarsystem and for Verlet the orbits of the planets become less stable, particularly for Mercury and Venus which are the two planets closest to the Sun.


\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4sunorigin1000years.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletsunorigin1000years.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Positions of Jupiter and Earth orbiting the Sun. The origin is not kept fixed.}
\label{fig:sunorigin}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4jupiterincr10-1000years.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletjupiterincr10-1000years.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Positions of Jupiter and Earth orbiting the Sun. Jupiter's mass is inreased by a factor 10. The origin is not kept fixed.}
\label{fig:incr10}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletx1000-02.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4x1000-02.pdf}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletx1000-04.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4x1000-04.pdf}
\end{subfigure}%
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletx1000-05.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4x1000-05.pdf}
\end{subfigure}%
\caption{The motion of the Sun, Earth and Jupiter with Jupiter's mass increased by a factor 1000 for three different time-steps. The green line is the Sun, blue is Earth and red is Jupiter.}
\label{fig:jupiterx1000}
\end{figure}


\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletmassorigin.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletmassoriginzoom.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Positions of Jupiter, Earth and Sun orbiting the center-of-mass of the system with zero total momentum. The figure to the right shows only the sun.}
\label{fig:massorigin}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletmassorigin1000years.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4massorigin1000years.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Positions of Jupiter, Earth and Sun orbiting the center-of-mass of the system with zero total momentum.}
\label{fig:massorigin1000}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletsolarsystem.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4solarsystemzoom.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Simulation of the solarsystem. The plot to the right is zoomed in on the four innermost planets.}
\label{fig:solarsystem}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Verletsolarsystemlargedt.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{RK4solarsystemlargedt.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Plot of the four innermost planets in the solarsystem for a large time-step.}
\label{fig:solarsystemlargedt}
\end{figure}

\newpage
\subsection*{Galactic Cluster}
In figure \ref{fig:3d} we see a simulation of a two-body problem in tree dimensions for RK4 and Velocity-Verlet for a large timestep. We see that Velocity-Verlet is unstable for this timestep, but RK4 gives a good result. Figure \ref{fig:longtime} shows the distance between the objects as a function of time for circular orbit. This gives a measurement of the error for the two methods. We see that they increase similarly with time.

The time usage for RK4 to advance one timestep is approximatly $2\times10^{-5}s$ and for Velocity-Verlet is $1.6\times10^{-5}s$.

Figure \ref{fig:system} shows the time evolution of a galactic cluster with 100 particles. We see that the particles starts with a random uniform distribution within a sphere and the system collapses at exactly $1\tau_{crunch}$. A few particles are then thrown out of the system while the rest of the system is in a state of equilibrium.

Table \ref{tab:2} shows the number of particles remaining in a system that starts with 100 particles, after different lengths of time and for different smoothing $\epsilon$'s. We see that for $\epsilon=1\times10^{-5}$ and smaller, the system does not reach an equilibrium and particles keep being ejected after several $\tau_{crunch}$. For $\epsilon=1\times10^{-4}$ and larger, the system becomes stable around $2\tau_{crunch}$.

In figure \ref{fig:energy} we see the total energy of the system as a function of time. The energy oscillates around the same point during the simulation except around $1\tau_{crunch}$ where the energy drops significantly. We also see that the total energy drops as particles are being ejected.

In figure \ref{fig:virial} we have calculated the virial theorem as a function of time for different number of particles, and compared the result to the theoretical expectation value for a system in equilibrium. 

Figure \ref{fig:rad} shows the radial density of particles in an equilibrium state for systems starting with different number of particles. We have tried to fit the distribution to the expression given by equation \ref{eq:rad}. The values we have used for $n_0$ and $r_0$ are given in the title of the plots.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{verletelliptical.pdf}
  %\caption{rk4}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rk4elliptical.pdf}
  %\caption{verlet}
  %\label{}
\end{subfigure}
\caption{Two-body problem in three dimensions for a large timestep.}
\label{fig:3d}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{longtime.pdf}
  \caption{Comparison of the Velocity-Verlet and Runge Kutta 4 methods for a long period of time. \label{fig:longtime}}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time0.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time0,5.pdf}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time1.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time1,5.pdf}
\end{subfigure}%
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time3.pdf}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{time7.pdf}
\end{subfigure}%
\caption{Time evolution of the galactic cluster. This simulation was run with a smoothing $\epsilon = 1\times10^{-3}$.}
\label{fig:system}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{energytot100.pdf}
  %\title{All particles in the system.}
  %\label{energya}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{energytot100r.pdf}
  %\caption*{Particles that have not been ejected.}
  %\label{energyb}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{energytot200.pdf}
  %\caption*{All particles in the system.}
  %\label{energya}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{energytot500.pdf}
  %\caption*{Particles that have not been ejected.}
  %\label{energyb}
\end{subfigure}
\caption{The total energy of the system as a function of time. The plot in the top left corner shows the energy for all the particles in the system. In the other plots particles are being ejected.}
\label{fig:energy}
\end{figure}

\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    & \multicolumn{5}{|c|}{Remaining particles} \\
    \hline
    $\tau_{crunch}$ & $\epsilon=1\times10^{-2}$ & $\epsilon=1\times10^{-3}$ & $\epsilon=1\times10^{-4}$ & $\epsilon=1\times10^{-5}$ & $\epsilon=0$ \\ \hline
    0 & 100 & 100 & 100 & 100 & 100 \\ \hline
    1.5 & 94 & 95 & 91 & 89 & 89 \\ \hline
    2 & 85 & 87 & 85 & 82 & 83 \\ \hline
    5 & 85 & 87 & 82 & 80 & 77 \\ \hline
    7 & 85 & 87 & 82 & 77 & 72 \\ \hline
  \end{tabular}
  \caption{Number of particles in the system after different lengths of time for different smoothing $\epsilon$'s. \label{tab:2}}
\end{table}
\clearpage

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{virial100.pdf}
  %\caption{Total energy for all particles}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{virial200.pdf}
  %\caption{Total energy for bound particles}
  %\label{}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{virial500.pdf}
  %\caption{Total energy for bound particles}
  %\label{}
\end{subfigure}
\caption{The virial theorem as a function of time for different number of particles.}
\label{fig:virial}
\end{figure}
\clearpage

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rad100.pdf}
  %\caption{Total energy for all particles}
  %\label{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rad200.pdf}
  %\caption{Total energy for bound particles}
  %\label{}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rad500.pdf}
  %\caption{Total energy for bound particles}
  %\label{}
\end{subfigure}
\caption{Radial density of particles in a system in equilibrium.}
\label{fig:rad}
\end{figure}

\newpage
\section*{Discussion}
\subsection*{Solarsystem}
In figure \ref{fig:circ} we see the Earth in circular orbit around the sun. The Earth was given an initial velocity of $v = 2\pi(1AU)/(1year)$. The two plots look identical, but in figure \ref{fig:dist10}, which shows the distance between the Earth and the Sun as a function of time, we see that there is a difference in accuracy between the two methods. The distance between the Sun and Earth varies a lot for the Verlet method compared to RK4 which is quite stable. For Verlet the distance oscillates around $1AU$ and the oscillation gets smaller with smaller time-step. For this time span the oscillation remains the same so the error doesn't increase. For RK4 however, we see that it starts with a great accuracy at $1AU$ but then gradually begins to oscillate and this oscillation only increases. 

Figure \ref{fig:dist5000} shows the same distance for 5000 years. We see that the error in RK4 increases gradually with time. Verlet starts out less accurate and then the error begins to oscillate but in general the error increases on the same scale as RK4.

It appears from this that RK4 gives a better accuracy for a short time span. Verlet requires a smaller timestep to get a more accurate result, but on a longer time scale the two methods seem to be equally stable.\\\\
Table \ref{tab:1} shows that the kinetic energy, potential energy and angular momentum of the Earth are constant, which is what we expect for the case of circular orbit. From equation \ref{eq:kin} we see that the kinetic energy depends on velocity which is constant for circular orbit. Equation \ref{eq:pot} shows that the potential energy depends on radial distance, which is also constant. The angular momentum, given in equation \ref{eq:ang}, depends on both velocity and radial distance. 

A planet bound in orbit around the Sun has a negative total energy. For the planet to escape from the Sun the total energy has to be positive. This means that the escape velocity is $v_{escape} > \sqrt{2GM_{\odot}/r}$. For a planet at a distance of $1AU$ from the sun this is $v_{escape} > 2\pi AU/year$\\\\
In figure \ref{fig:sunorigin} Jupiter has been added to the simulation. The Sun was initially placed at the origin with no initial velocity. We see that adding Jupiter gives the whole system a momentum. RK4 gives a better result than Verlet in this case. For Verlet the orbit of Earth becomes unstable and the method requires a smaller time step to give the same result as RK4.

Figure \ref{fig:incr10} shows the same system but with Jupiter's mass increased by a factor 10. The system is now given a larger momentum and travels farther during the same length of time. Again for Verlet the orbit of the Earth becomes a little unstable whereas for RK4 it moves upwards in a straight line.

In figure \ref{fig:jupiterx1000} the mass of jupiter has been increased by a factor 1000. Neither of the methods manage to calculate a stable motion for the system. The Earth escapes after only a short period of time in all the plots. For the shortest time step we have plotted here, Earth manages one more orbit around the Sun than for the second shortest time step. The results are similar for both methods. The only difference is that the Earth escapes with slightly different velocity and direction. Interestingly, for the largest time step that we have calculated, it appears that Verlet gives a much better result than for the smaller time steps. The Earth does many more orbits around the Sun before it escapes. However, this is probably only due to the fact that for a large time step we skip the problematic points in the calculation. So this is only due to luck and does not mean that the method is stable for this given timestep.\\\\
The three-body problem with the center-of-mass as origin is shown in figure \ref{fig:massorigin} and \ref{fig:massorigin1000}. Here the Sun also moves in a circular orbit. For a short time span both methods give good results but if we run the simulation for a long time the orbit of the Earth becomes more unstable for Verlet than for RK4. Again Verlet requires a smaller time step in order to give equally good result as RK4.\\\\
Figure \ref{fig:solarsystem} and \ref{fig:solarsystemlargedt} show simulations of the whole solarsystem with circular orbits. For a short time step both methods appear to be stable. For a larger time step we see that for Verlet the orbits of the innermost planets become unstable with time, however they all stay in orbit for the duration of this simulation. For RK4 the innermost planet, Mercury, falls in towards the Sun and suddenly escapes the solarsystem. The orbits of the other planets appear stable. In this case both methods require the smaller timestep but Verlet is more stable than RK4.

\subsection*{Galactic Cluster}
From figures \ref{fig:3d} and \ref{fig:longtime} we see that RK4 is more accurate than Velocity-Verlet for a large timestep, and for a long period of time with a smaller timestep both methods seem to be equally stable. We chose to continue running our program using RK4 even though it is more time consuming, because with the adaptive timestep method the timestep will become larger at times and then RK4 will give a more accurate result than Velocity-Verlet.\\\\
From figure \ref{fig:system} we see that the system collapses at $1 \tau_{crunch}$ which is what we expect from theory. It does not collapse into a singularity however. This is because we simulate point particles which means we get elastic collisions, so the particles do not stick together but continue with the same kinetic energy as before the collision.\\\\
From table \ref{tab:2} we see that a smoothing $\epsilon=1\times10^{-3}$ gives the most stable system. Few particles escape and the system reaches equilibrium within $2\tau_{crunch}$. We therefore ran the rest of the simulations using this $\epsilon$.

The reason we introduce this smoothing function is because we simulate point particles and we see from equation \ref{eq:force} that the force can become infinitely large if the particles come infinitely close to each other. The smoothing function will make sure this does not happen.\\\\
From figure \ref{fig:energy} we see that energy is carried away by the particles that are ejected from the system. We see that for the whole system the energy is conserved, it fluctuates but does not change considerably with time. Just before and around $1\tau_{crunch}$ there is a significant drop in the energy. This is where the system collapses, and when the particles get very close to each other the code struggles to give accurate results. The potential energy (equation \ref{eq:pot}) becomes very large when $r$ becomes small. For the force this was taken care of by introducing the smoothing function but the particles can still get very close resulting in a large potential energy. We get similar results for different number of particles.\\\\
Figure \ref{fig:virial} shows the virial theorem for different numbers of particles as a function of time. The values get close to the expectation value for a system in equilibrium shortly after $1\tau_{crunch}$. We get similar curves for the different numbers of particles. The system starts with zero kinetic energy and it has a peak at $1\tau_{crunch}$ where the particles are closest. The system then stabilizes close to the expected value. From this we can conclude that the system reaches an equilibrium shortly after $1\tau_{crunch}$. It appears that the system with a large number of particles reaches equilibrium earlier than the systems with fewer particles.\\\\
The radial density of particles in an equilibrium state is shown in figure \ref{fig:rad}. We see that most particles are within $0.5r_0$ with a few particles as far out as $3.5r_0$. With $n_0$ equal to half the number of particles that the system starts out with, and $r_0$ equal to half the radius of the sphere that the particles were initially distributed in, we seem to get a pretty good fit.

\end{document}
