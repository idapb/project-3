The aim of this project is to first simulate the solar system using Newtonian gravity. This will be solved with the Runge-Kutta 4 method and the Verlet method, in order to compare the stability and precision of these two methods.

We will then modify our program to simulate a galactic cluster with a large number of
particles. The aim is to look at a simple model for an open cluster made from the gravita-
tional collapse and interaction among a large number of stars. We will study the statistical properties of the collapsed system.